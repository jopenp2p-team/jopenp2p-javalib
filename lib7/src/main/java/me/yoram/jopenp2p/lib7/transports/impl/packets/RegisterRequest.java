/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.impl.packets;

import me.yoram.jopenp2p.lib7.transports.api.AbstractServerRequest;
import me.yoram.jopenp2p.lib7.transports.api.PacketPrefix;
import me.yoram.jopenp2p.lib7.transports.api.EPacketType;
import me.yoram.jopenp2p.lib7.utils.ObjectUtils;
import me.yoram.jopenp2p.lib7.utils.ResizeableByteBuffer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 13/07/18
 */
@PacketPrefix(EPacketType.REGISTER_REQUEST)
public class RegisterRequest extends AbstractServerRequest {
    private String username;

    public String getUsername() {
        return username;
    }

    public RegisterRequest username(final String username) {
        this.username = username;

        return this;
    }

    @Override
    protected short getLength() {
        return (short)(super.getLength() + this.username.length() + 2);
    }

    @Override
    public RegisterRequest transactionId(long transactionId) {
        return (RegisterRequest)super.transactionId(transactionId);
    }

    @Override
    public RegisterRequest appid(String appid) {
        return (RegisterRequest)super.appid(appid);
    }

    @Override
    public RegisterRequest authid(String authid) {
        return (RegisterRequest)super.authid(authid);
    }

    @Override
    protected void load(final ResizeableByteBuffer buffer) throws IOException {
        super.load(buffer);
        this.username = buffer.getString();
    }

    @Override
    protected void save(final ResizeableByteBuffer buffer) {
        super.save(buffer);
        buffer.putString(this.username);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RegisterRequest) {
            final RegisterRequest o = (RegisterRequest)obj;

            return super.equals(obj) &&
                    Objects.equals(this.username, o.username);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return ObjectUtils.hashCode(super.hashCode(), this.username);
    }
}
