/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.impl.packets.dial;

import me.yoram.jopenp2p.lib7.transports.api.PacketPrefix;
import me.yoram.jopenp2p.lib7.transports.api.EPacketType;
import me.yoram.jopenp2p.lib7.utils.ResizeableByteBuffer;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 17/07/18
 */
@PacketPrefix(EPacketType.CALL_BACK_REQUEST)
public class CallbackRequest extends DialResponse {
    @Override
    public CallbackRequest transactionId(long transactionId) {
        return (CallbackRequest)super.transactionId(transactionId);
    }

    @Override
    public CallbackRequest remoteUsername(final String remoteUsername) {
        return (CallbackRequest)super.remoteUsername(remoteUsername);
    }

    @Override
    public CallbackRequest config(final ResizeableByteBuffer.IPConfig config) {
        return (CallbackRequest)super.config(config);
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof CallbackRequest && super.equals(obj));
    }
}
