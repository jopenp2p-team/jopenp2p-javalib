/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.tasks.register;

import me.yoram.jopenp2p.lib7.api.RendezVousConfig;
import me.yoram.jopenp2p.lib7.transports.impl.packets.RegisterRequest;

import java.util.TimerTask;

/**
 * register-udp (0x01)
 * applicationid (16 bytes)
 * blocktype -
 *      "internalip 0x01"
 *          ip (4 byte)
 *          port (2 byte)
 *      "username 0x02"
 *          username-len (1 byte)
 *          username (0-255)
 *
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 11/07/18
 */
public class RegisterClientTask extends TimerTask {
    private final RendezVousConfig config;

    public RegisterClientTask(final RendezVousConfig config) {
        super();

        if (config  == null) {
            throw new IllegalArgumentException("config parameter is null");
        }

        this.config = config;
    }

    @Override
    public void run() {
        if (config.getHost() != null && config.getPort() > 0 && config.getPort() < 65536) {
            try {
                final RegisterRequest packet = new RegisterRequest()
                        .username(config.getUsername())
                        .appid(config.getAppId())
                        .authid(config.getAuthId())
                        .transactionId(System.currentTimeMillis());
                config.getSocket().send(packet, config.getHost(), config.getPort());
            } catch (Throwable t) {
                // do nothing
            }
        }
    }
}
