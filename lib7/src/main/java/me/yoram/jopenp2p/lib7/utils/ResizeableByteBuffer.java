/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.utils;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;

public class ResizeableByteBuffer {
    /**
     * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
     * @since 15/07/18
     */
    public enum EStreamerHeader {
        STRING_HEADER(1, "String Block"),
        IP_HEADER(2, "IP/PORT Block"),
        BYTE_ARRAY(3, "Byte Array");

        private final int value;
        private final String name;

        EStreamerHeader(final int value, String name) {
            this.value = value;
            this.name = name;
        }

        public int getValue() {
            return value;
        }

        public String getName() {
            return name;
        }


        public static EStreamerHeader fromValue(final int value) {
            for (final EStreamerHeader o: EStreamerHeader.values()) {
                if (o.value == value) {
                    return o;
                }
            }

            return null;
        }
    }

    /**
     * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
     * @since 15/07/18
     */
    public static class IPConfig {
        private InetAddress address;
        private int port;

        public InetAddress getAddress() {
            return address;
        }

        public IPConfig address(final InetAddress address) {
            this.address = address;

            return this;
        }

        public int getPort() {
            return port;
        }

        public IPConfig port(final int port) {
            this.port = port;

            return this;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof IPConfig) {
                IPConfig o = (IPConfig)obj;

                return Objects.equals(address, o.address) && port == o.port;
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            return ObjectUtils.hashCode(0, address, port);
        }

        @Override
        public String toString() {
            return String.format("{%s/%d}", this.address == null ? "null" : this.address.getHostAddress(), port);
        }
    }

    private static final int MAX_STRING_SIZE = 255;

    public static void checkRecordIs(final int expected, final int actual) throws IOException {
        if (expected != actual) {
            final EStreamerHeader actualHeader = EStreamerHeader.fromValue(actual);

            if (actualHeader == null) {
                throw new IOException(String.format("Record of type %d is unknown", actual));
            }

            final EStreamerHeader expectedHeader = EStreamerHeader.fromValue(expected);

            if (expectedHeader == null) {
                throw new IOException(String.format("Record of type %d is unknown", actual));
            }

            throw new IOException(
                    String.format(
                            "Expected record of type %s but %s found.",
                            expectedHeader.getValue(),
                            actualHeader.getName()));
        }
    }

    private ByteBuffer buffer;
    private final int blockSize;
    private final boolean readOnly;

    public ResizeableByteBuffer(int blockSize) {
        super();

        this.buffer = ByteBuffer.allocate(blockSize);
        this.blockSize = blockSize;
        this.readOnly = false;
    }

    public ResizeableByteBuffer(byte[] data, final int offset, final int length) {
        super();

        this.buffer = ByteBuffer.wrap(data, offset, length);
        this.blockSize = 0;
        this.readOnly = true;
    }

    public ResizeableByteBuffer(byte[] data) {
        this(data, 0, data.length);
    }

    public void put(byte b) {
        if (readOnly) {
            throw new RuntimeException("Readonly buffer");
        }

        growIfNeeded(1);
        this.buffer.put(b);
    }

    public void putByteArray(byte[] b) {
        if (readOnly) {
            throw new RuntimeException("Readonly buffer");
        }

        if (b != null && b.length > Short.MAX_VALUE) {
            throw new IllegalArgumentException(String.format("Maximum Byte Array size is %d", b.length));
        }

        growIfNeeded(b == null ? 3 : b.length + 3);
        this.buffer.put((byte)EStreamerHeader.BYTE_ARRAY.value);
        this.buffer.putShort(b == null ? (short)0 : (short)b.length);
        if (b != null && b.length != 0) {
            this.buffer.put(b);
        }
    }

    public void putShort(short s) {
        if (readOnly) {
            throw new RuntimeException("Readonly buffer");
        }

        growIfNeeded(2);
        this.buffer.putShort(s);
    }

    public void putInt(int i) {
        if (readOnly) {
            throw new RuntimeException("Readonly buffer");
        }

        growIfNeeded(4);
        this.buffer.putInt(i);
    }

    public void putLong(final long l) {
        if (readOnly) {
            throw new RuntimeException("Readonly buffer");
        }

        growIfNeeded(8);

        this.buffer.putLong(l);
    }

    public void putString(final String s) {
        if (readOnly) {
            throw new RuntimeException("Readonly buffer");
        }

        if (s != null && s.length() > MAX_STRING_SIZE) {
            throw new IllegalArgumentException(String.format("Maximum String size is %d", s.length()));
        }

        put((byte)EStreamerHeader.STRING_HEADER.getValue());
        put(s == null ? 0 : (byte)s.length());

        if (s != null) {
            this.buffer.put(s.getBytes());
        }
    }

    public void putIpConfig(IPConfig config) {
        if (readOnly) {
            throw new RuntimeException("Readonly buffer");
        }

        put((byte)EStreamerHeader.IP_HEADER.getValue());
        this.buffer.put(config.getAddress().getAddress());
        putShort((short)config.getPort());
    }

    public byte get() {
        return this.buffer.get();
    }

    public byte[] getByteArray() throws IOException {
        checkRecordIs(EStreamerHeader.BYTE_ARRAY.getValue(), get());
        int len = (int)getShort() & 0xFFFF;
        if (len < 0) {
            throw new IOException("Negative length for Byte Array");
        }

        if (len > Short.MAX_VALUE) {
            throw new IOException(
                    String.format(
                            "The Byte Array looks like it is %d bytes but the maximum size is %d",
                            len,
                            Short.MAX_VALUE));
        }

        if (len > 0) {
            byte[] buf = new byte[len];
            this.buffer.get(buf);
            return buf;
        } else {
            return null;
        }
    }

    public short getShort() {
        return this.buffer.getShort();
    }

    public int getInt() {
        return this.buffer.getInt();
    }

    public long getLong() {
        return this.buffer.getLong();
    }

    public String getString() throws IOException {
        checkRecordIs(EStreamerHeader.STRING_HEADER.getValue(), get());
        int len = (int)get() & 0xFF;
        if (len < 0) {
            throw new IOException("Negative length for String");
        }

        if (len > MAX_STRING_SIZE) {
            throw new IOException(
                    String.format(
                            "The String looks like it is %d bytes but the maximum size is %d",
                            len,
                            MAX_STRING_SIZE));
        }

        if (len > 0) {
            byte[] buf = new byte[len];
            this.buffer.get(buf);
            return new String(buf);
        } else {
            return null;
        }
    }

    public IPConfig getIpConfig() throws IOException {
        checkRecordIs(EStreamerHeader.IP_HEADER.getValue(), get());

        byte[] buf = new byte[4];
        this.buffer.get(buf);
        int port = getShort() & 0xFFFF;

        return new IPConfig().address(InetAddress.getByAddress(buf)).port(port);
    }

    public Object getNext() throws IOException {
        if (this.buffer.remaining() == 0) {
            return null;
        }

        int currentPos = this.buffer.position();
        EStreamerHeader header = EStreamerHeader.fromValue(get());
        if (header == null) {
            return null;
        }

        this.buffer.position(currentPos);
        switch (header) {
            case STRING_HEADER:
                return getString();

            case IP_HEADER:
                return getIpConfig();

             default:
                 return null;
        }
    }

    public byte[] getData() {
        return Arrays.copyOf(this.buffer.array(), this.buffer.position());
    }

    private void growIfNeeded(int needed) {
        if (this.buffer.remaining() < needed) {
            final int sizeToAdd = needed - this.buffer.remaining();
            final int blocksToAdd = sizeToAdd / this.blockSize + (sizeToAdd % this.blockSize == 0 ? 0 : 1);

            final ByteBuffer b = ByteBuffer.allocate(this.buffer.capacity() + (blocksToAdd * this.blockSize));
            b.put(this.buffer);
            this.buffer = b;
        }
    }
}
