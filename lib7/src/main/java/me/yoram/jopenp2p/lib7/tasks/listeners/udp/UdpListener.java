/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.tasks.listeners.udp;

import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 17/07/18
 */
public class UdpListener implements Runnable, SocketBinder {
    private DatagramSocket socket;

    public UdpListener(final int port) throws IOException {
        super();

        this.socket = port == -1 ? new DatagramSocket() : new DatagramSocket(port);
    }

    @Override
    public void run() {
        final byte[] buf = new byte[1024];

        while (true) {
            try {
                DatagramPacket p = new DatagramPacket(buf, buf.length);
                socket.receive(p);

                if (p.getLength() == 0) {
                    continue;
                }

                AbstractPacket.notify(p.getData(), p.getOffset(), p.getLength(), p.getAddress(), p.getPort(), this);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

    }

    private void send(final byte[] buf, InetAddress host, int port) throws IOException {
        final DatagramPacket packet = new DatagramPacket(buf, buf.length, host, port);
        socket.send(packet);
    }

    @Override
    public int getPort() {
        return socket.getPort();
    }

    @Override
    public String getHostAddress() {
        return socket.getLocalAddress().getHostAddress();
    }

    @Override
    public void send(final AbstractPacket p, InetAddress host, int port) throws IOException {
        send(p.save(), host, port);
    }
}
