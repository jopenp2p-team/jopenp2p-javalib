/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.impl.packets.list;

import me.yoram.jopenp2p.lib7.transports.api.EPacketType;
import me.yoram.jopenp2p.lib7.transports.api.PacketPrefix;
import me.yoram.jopenp2p.lib7.transports.impl.packets.dial.DialResponse;
import me.yoram.jopenp2p.lib7.utils.ResizeableByteBuffer;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 13/07/18
 */
@PacketPrefix(EPacketType.LIST_RESPONSE)
public class ListResponse extends DialResponse {
    @Override
    public ListResponse transactionId(long transactionId) {
        return (ListResponse)super.transactionId(transactionId);
    }

    @Override
    public ListResponse remoteUsername(String remoteUsername) {
        return (ListResponse)super.remoteUsername(remoteUsername);
    }

    @Override
    public ListResponse config(ResizeableByteBuffer.IPConfig config) {
        return (ListResponse)super.config(config);
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof ListResponse && super.equals(obj));
    }
}
