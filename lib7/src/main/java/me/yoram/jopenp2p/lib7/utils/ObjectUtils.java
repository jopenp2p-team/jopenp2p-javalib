package me.yoram.jopenp2p.lib7.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Utility functions
 *
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 13/06/18
 */
public class ObjectUtils {
    private ObjectUtils() {}

    /**
     * This is a hashcode function is reusable
     *
     * @param intialResult this value would be zero for the root object, otherwise it is super.hashCode()
     * @param objects a list of obects, in order, to calculate the hashcode
     * @return the hashcode
     */
    public static int hashCode(final int intialResult, final Object... objects) {
        int res = intialResult;

        for (final Object o: objects) {
            if (o != null) {
                if (o.getClass().isArray()) {
                    Object[] arr = (Object[]) o;
                    for (Object obj : arr) {
                        res = 31 * res + obj.hashCode();
                    }
                } else {
                    res = 31 * res + o.hashCode();
                }
            }
        }

        return res;
    }

    public static Collection<String> inputStreamToStringCollection(final InputStream in) throws IOException {
        if (in == null) {
            return null;
        }

        final List<String> res = new ArrayList<>();

        try (
                final InputStreamReader reader = new InputStreamReader(in);
                final BufferedReader bufferedReader = new BufferedReader(reader)
        ) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                res.add(line);
            }
        }

        return res;
    }

    public static Collection<String> resourceToStringCollection(final String resourceName) throws IOException {
        try (final InputStream in = ObjectUtils.class.getResourceAsStream(resourceName)) {
            return inputStreamToStringCollection(in);
        }
    }
}
