/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.impl.events;

import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.api.PacketEvent;
import me.yoram.jopenp2p.lib7.transports.impl.packets.echo.EchoRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.echo.EchoResponse;

import java.io.IOException;
import java.net.InetAddress;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 19/07/18
 */
public class SimpleEchoRequestEvent implements PacketEvent {
    @Override
    public void onPacket(AbstractPacket p, InetAddress host, int port, SocketBinder binder) throws IOException {
        if (p instanceof EchoRequest) {
            final EchoResponse resp = new EchoResponse()
                    .transactionId(p.getTransactionId())
                    .data(((EchoRequest) p).getData());

            binder.send(resp, host, port);
        }
    }
}
