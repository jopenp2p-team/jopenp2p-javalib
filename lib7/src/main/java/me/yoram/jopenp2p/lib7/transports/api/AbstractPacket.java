/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.api;

import me.yoram.jopenp2p.lib7.api.PacketEvent;
import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.lib7.utils.ObjectUtils;
import me.yoram.jopenp2p.lib7.utils.ResizeableByteBuffer;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 13/07/18
 */
public abstract class AbstractPacket {
    private static final Map<EPacketType, Class<?>> MAP_PER_ENUM = new HashMap<>();
    private static final Map<EPacketType, List<WeakReference<PacketEvent>>> EVENTS = new ConcurrentHashMap<>();
    private static final Map<Long, WeakReference<PacketEvent>> EVENTS_FOR_TRANS = new ConcurrentHashMap<>();

    static {
        try {
            Collection<String> classes = ObjectUtils.resourceToStringCollection(
                    "/META-INF/services/me.yoram.jopenp2p.lib7.transports.api.AbstractPacket");

            for (String clazz: classes) {
                Class<?> c = Class.forName(clazz);

                PacketPrefix o = c.getAnnotation(PacketPrefix.class);

                MAP_PER_ENUM.put(o.value(), c);
            }

            for (EPacketType pt: MAP_PER_ENUM.keySet()) {
                EVENTS.put(pt, new CopyOnWriteArrayList<WeakReference<PacketEvent>>());
            }

            new Thread(new Runnable() {
                @Override
                public void run() {
                    Thread.currentThread().setName("Cleaning events that have been garbage collected");
                    final List<WeakReference<PacketEvent>> holder = new ArrayList<>();
                    final List<Long> holder2 = new ArrayList<>();

                    while (true) {
                        try {
                            for (List<WeakReference<PacketEvent>> list: EVENTS.values()) {
                                holder.clear();

                                for (WeakReference<PacketEvent> weakRef: list) {
                                    if (weakRef.get() == null) {
                                        holder.add(weakRef);
                                    }
                                }

                                for (WeakReference<PacketEvent> weakRef: holder) {
                                    list.remove(weakRef);
                                }
                            }

                            holder2.clear();

                            for (Map.Entry<Long, WeakReference<PacketEvent>> entry: EVENTS_FOR_TRANS.entrySet()) {
                                if (entry.getValue().get() == null) {
                                    holder2.add(entry.getKey());
                                }
                            }

                            for (Long l: holder2) {
                                EVENTS_FOR_TRANS.remove(l);
                            }

                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            break;
                        } catch (Throwable t) {
                            // DO NOTHING
                        }
                    }
                }
            }).start();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static Map<EPacketType, Class<?>> getMapPerEnum() {
        return new HashMap<>(MAP_PER_ENUM);
    }

    public static void registerEvent(final EPacketType packetType, final PacketEvent event) {
        List<WeakReference<PacketEvent>> list = EVENTS.get(packetType);

        if (list != null) {
            list.add(new WeakReference<>(event));
        }
    }

    public static void registerEvent(final EPacketType[] packetTypes, final PacketEvent event) {
        for (EPacketType pt: packetTypes) {
            registerEvent(pt, event);
        }
    }

    public static void registerEvent(final long transaction, final PacketEvent event) {
        EVENTS_FOR_TRANS.put(transaction, new WeakReference<>(event));
    }

    public static void unregisterEvent(final EPacketType packetType, final PacketEvent event) {
        if (EVENTS.containsKey(packetType)) {
            List<WeakReference<PacketEvent>> list = EVENTS.get(packetType);

            for (WeakReference<PacketEvent> weakRef: list) {
                Object o = weakRef.get();

                if (o == event) {
                    list.remove(weakRef);

                    return;
                }
            }
        }
    }

    public static void unregisterEvent(final EPacketType[] packetTypes, final PacketEvent event) {
        for (EPacketType pt: packetTypes) {
            unregisterEvent(pt, event);
        }
    }

    public static void unregisterEvent(final long transaction) {
        EVENTS_FOR_TRANS.remove(transaction);
    }

    public static void notify(
            final byte[] buf,
            final int offset,
            final int length,
            final InetAddress remoteHost,
            final int remotePort,
            final SocketBinder binder) {
        if (length == 0) {
            return;
        }

        final EPacketType pt = EPacketType.fromValue(buf[offset]);

        if (pt != null && EVENTS.containsKey(pt)) {
            try {
                final List<WeakReference<PacketEvent>> list = EVENTS.get(pt);
                final Class<?> clazz = MAP_PER_ENUM.get(pt);
                final AbstractPacket o = (AbstractPacket)clazz.newInstance();
                o.load(buf, offset, length);

                for (final WeakReference<PacketEvent> weakRef: list) {
                    final PacketEvent event = weakRef.get();

                    if (event != null) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    event.onPacket(o, remoteHost, remotePort, binder);
                                } catch (Throwable t) {
                                    // DO NOTHING
                                }

                            }
                        }).start();
                    }
                }

                final WeakReference<PacketEvent> transPacket = EVENTS_FOR_TRANS.get(o.getTransactionId());
                if (transPacket != null) {
                    final PacketEvent transEvt = transPacket.get();

                    if (transEvt != null) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    transEvt.onPacket(o, remoteHost, remotePort, binder);
                                } catch (Throwable t) {
                                    // DO NOTHING
                                }

                            }
                        }).start();
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
                // DO NOTHING
            }
        }
    }

    static Map<EPacketType, List<WeakReference<PacketEvent>>> getEVENTS() {
        return EVENTS;
    }

    private byte version = 1;
    private long transactionId;
    private long packetId = 0;

    public byte getVersion() {
        return version;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public AbstractPacket transactionId(final long transactionId) {
        this.transactionId = transactionId;

        return this;
    }

    public long getPacketId() {
        return packetId;
    }

    public AbstractPacket packetId(long packetId) {
        this.packetId = packetId;

        return this;
    }

    public void load(byte[] buf, int offset, int length) throws IOException {
        final EPacketType prefix = getPacketPrefix();

        final ResizeableByteBuffer buffer = new ResizeableByteBuffer(buf, offset, length);

        byte b = buffer.get();

        if (b != prefix.getValue()) {
            throw new IOException(String.format("Packet of type %d is not %s packet", b, prefix.toString()));
        }

        b = buffer.get();

        if (b != 1) {
            throw new IOException(
                    String.format("Packet of type %d version %d is unknown", prefix.getValue(), b));
        }

        this.transactionId = buffer.getLong();
        this.packetId = buffer.getLong();
        buffer.getShort();

        load(buffer);
    }


    public byte[] save() throws IOException {
        final ResizeableByteBuffer buffer = new ResizeableByteBuffer(512);
        buffer.put(getPacketPrefix().getValue());
        buffer.put(version);
        buffer.putLong(this.transactionId);
        buffer.putLong(this.packetId);
        buffer.putShort(getLength());
        save(buffer);

        return buffer.getData();

    }

    protected abstract void load(ResizeableByteBuffer buffer) throws IOException;
    protected abstract void save(ResizeableByteBuffer buffer) throws IOException;
    protected abstract short getLength() throws IOException;

    private EPacketType getPacketPrefix() throws IOException {
        final PacketPrefix prefix = this.getClass().getAnnotation(PacketPrefix.class);

        if (prefix == null) {
            throw new IOException("PacketType not defined, add PacketPrefix annotation to class");
        }

        return prefix.value();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AbstractPacket) {
            final AbstractPacket o = (AbstractPacket)obj;

            return transactionId == o.transactionId && version == o.version;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return ObjectUtils.hashCode(0, transactionId, version);
    }

    @Override
    public String toString() {
        try {
            final Map<String, Object> props = new TreeMap<>();

            Class<?> currentClass = this.getClass();

            while (currentClass != null && !currentClass.equals(Object.class)) {
                for (final Field f: currentClass.getDeclaredFields()) {
                    if (!Modifier.isStatic(f.getModifiers())) {
                        f.setAccessible(true);
                        props.put(f.getName(), f.get(this));
                    }
                }

                currentClass = currentClass.getSuperclass();
            }

            return props.toString();
        } catch (Throwable t) {
            return String.format("%s: %s", t.getClass().getName(), t.getMessage());
        }
    }
}
