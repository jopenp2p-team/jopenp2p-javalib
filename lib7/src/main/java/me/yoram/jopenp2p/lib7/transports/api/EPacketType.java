/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.api;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 18/07/18
 */
public enum EPacketType {
    SIGN_IN((byte)1),
    REGISTER_REQUEST((byte)2),
    DIAL_REQUEST((byte)3),
    DIAL_RESPONSE((byte)4),
    CALL_BACK_REQUEST((byte)5),
    CONNECTED((byte)6),
    HELLO((byte)7),
    LIST_REQUEST((byte)8),
    LIST_RESPONSE((byte)9),
    ECHO_REQUEST((byte)10),
    ECHO_RESPONSE((byte)11),
    APPLICATION((byte)100);

    public static EPacketType fromValue(final byte value) {
        for (EPacketType pt: EPacketType.values()) {
            if (pt.value == value) {
                return pt;
            }
        }

        return null;
    }

    private final byte value;

    EPacketType(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return value;
    }
}
