/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.api;

import me.yoram.jopenp2p.lib7.utils.ObjectUtils;
import me.yoram.jopenp2p.lib7.utils.ResizeableByteBuffer;

import java.io.IOException;
import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 19/07/18
 */
public abstract class AbstractServerRequest extends AbstractPacket {
    private String authid;
    private String appid;

    public String getAuthid() {
        return authid;
    }

    public AbstractServerRequest authid(final String authid) {
        this.authid = authid;

        return this;
    }

    public String getAppid() {
        return appid;
    }

    public AbstractServerRequest appid(final String appid) {
        this.appid = appid;

        return this;
    }

    @Override
    protected short getLength() {
        return (short)(this.appid.length() + 2 + this.authid.length() + 2);
    }

    @Override
    protected void load(final ResizeableByteBuffer buffer) throws IOException  {
        this.authid = buffer.getString();
        this.appid = buffer.getString();
    }

    @Override
    protected void save(final ResizeableByteBuffer buffer) {
        buffer.putString(this.authid);
        buffer.putString(this.appid);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AbstractServerRequest) {
            final AbstractServerRequest o = (AbstractServerRequest)obj;

            return super.equals(o) && Objects.equals(this.appid, o.appid) && Objects.equals(this.authid, authid);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return ObjectUtils.hashCode(super.hashCode(), appid, authid);
    }
}
