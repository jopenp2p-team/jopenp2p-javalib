/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.impl.packets.dial;

import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.transports.api.EPacketType;
import me.yoram.jopenp2p.lib7.transports.api.PacketPrefix;
import me.yoram.jopenp2p.lib7.utils.ObjectUtils;
import me.yoram.jopenp2p.lib7.utils.ResizeableByteBuffer;

import java.io.IOException;
import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 17/07/18
 */
@PacketPrefix(EPacketType.DIAL_RESPONSE)
public class DialResponse extends AbstractPacket {
    private String remoteUsername;
    private ResizeableByteBuffer.IPConfig config;

    public String getRemoteUsername() {
        return remoteUsername;
    }

    public DialResponse remoteUsername(final String remoteUsername) {
        this.remoteUsername = remoteUsername;

        return this;
    }

    public ResizeableByteBuffer.IPConfig getConfig() {
        return config;
    }

    public DialResponse config(final ResizeableByteBuffer.IPConfig config) {
        this.config = config;

        return this;
    }

    @Override
    protected short getLength() {
        return (short)(remoteUsername.length() + 2 + 8);
    }

    @Override
    public DialResponse transactionId(long transactionId) {
        return (DialResponse)super.transactionId(transactionId);
    }

    @Override
    protected void load(final ResizeableByteBuffer buffer) throws IOException {
        remoteUsername = buffer.getString();
        config = buffer.getIpConfig();
    }

    @Override
    protected void save(final ResizeableByteBuffer buffer) {
        buffer.putString(remoteUsername);
        buffer.putIpConfig(config);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DialResponse) {
            final DialResponse o = (DialResponse)obj;

            return super.equals(obj) &&
                    Objects.equals(config, o.config) &&
                    Objects.equals(remoteUsername, o.remoteUsername);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return ObjectUtils.hashCode(super.hashCode(), config, remoteUsername);
    }
}
