/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.impl.events;

import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.api.PacketEvent;

import java.io.*;
import java.net.InetAddress;
import java.util.UUID;

/**
 * Log all events received to a output stream and potentially the packet to a separate files.
 *
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 19/07/18
 */
public class SimpleDebugLoggerEvents implements PacketEvent {
    private File logPacketFolder;
    private final PrintStream stream;

    public SimpleDebugLoggerEvents(final PrintStream stream) {
        super();

        this.stream = stream;
    }

    public SimpleDebugLoggerEvents logPacketFolder(final File logPacketFolder) {
        if (logPacketFolder != null) {
            if (!logPacketFolder.exists() && !logPacketFolder.mkdirs()) {
                throw new IllegalArgumentException(
                        String.format(
                                "The log folder %s do not exists and cannot be created.",
                                logPacketFolder.getAbsolutePath()));
            } else if (!logPacketFolder.isDirectory()) {
                throw new IllegalArgumentException(
                        String.format("The log folder %s is not a valid directory", logPacketFolder.getAbsolutePath()));
            }
        }

        this.logPacketFolder = logPacketFolder;

        return this;
    }

    @Override
    public void onPacket(
            final AbstractPacket p,
            final InetAddress host,
            final int port,
            final SocketBinder binder) throws IOException {
        final String packetFileMsg;
        if (logPacketFolder != null) {
            final File f = new File(this.logPacketFolder, UUID.randomUUID().toString() + ".packet");

            final byte[] data = p.save();

            try (OutputStream out = new FileOutputStream(f)) {
                out.write(data);
            }

            packetFileMsg = String.format(" The packet has been stored in %s", f.getAbsolutePath());
        } else {
            packetFileMsg = String.format(" Packet Content: %s", p.toString().replace("\n", ""));
        }

        stream.println(
                String.format(
                        "Packet of type %s received from %s/%d.%s",
                        p.getClass().getSimpleName(),
                        host.getHostAddress(),
                        port,
                        packetFileMsg));
        stream.flush();
    }
}
