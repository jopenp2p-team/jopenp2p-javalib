/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.impl.packets;

import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.transports.api.PacketPrefix;
import me.yoram.jopenp2p.lib7.transports.api.EPacketType;
import me.yoram.jopenp2p.lib7.utils.ObjectUtils;
import me.yoram.jopenp2p.lib7.utils.ResizeableByteBuffer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 13/07/18
 */
@PacketPrefix(EPacketType.APPLICATION)
public class ApplicationPacket extends AbstractPacket {
    private String remoteUsername;
    private byte[] data;

    public String getRemoteUsername() {
        return remoteUsername;
    }

    public ApplicationPacket remoteUsername(final String remoteUsername) {
        this.remoteUsername = remoteUsername;

        return this;
    }

    public byte[] getData() {
        return data;
    }

    public ApplicationPacket data(final byte[] data) {
        this.data = data;

        return this;
    }

    @Override
    protected short getLength() {
        return (short)(this.remoteUsername.length() + 2 + data.length + 3);
    }

    @Override
    public ApplicationPacket transactionId(long transactionId) {
        return (ApplicationPacket)super.transactionId(transactionId);
    }

    @Override
    protected void load(final ResizeableByteBuffer buffer) throws IOException {
        this.remoteUsername = buffer.getString();
        this.data = buffer.getByteArray();
    }

    @Override
    public void save(ResizeableByteBuffer buffer) {
        buffer.putString(this.remoteUsername);
        buffer.putByteArray(data);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ApplicationPacket) {
            final ApplicationPacket o = (ApplicationPacket)obj;

            return super.equals(obj) &&
                    Arrays.equals(this.data, o.data) &&
                    Objects.equals(this.remoteUsername, o.remoteUsername);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return ObjectUtils.hashCode(super.hashCode(), data, remoteUsername);
    }
}
