/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.tasks.dialer;

import me.yoram.jopenp2p.lib7.api.PacketEvent;
import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.transports.impl.packets.dial.DialResponse;
import me.yoram.jopenp2p.lib7.transports.impl.packets.echo.EchoRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.echo.EchoResponse;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Set;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 29/07/18
 */
public class DialingEvents implements PacketEvent {
    private final Set<String> opt;
    private final long transactionId;

    public DialingEvents(Set<String> opt, long transactionId) {
        this.opt = opt;
        this.transactionId = transactionId;
    }

    @Override
    public void onPacket(AbstractPacket p, InetAddress host, int port, final SocketBinder binder) throws IOException {
        if (p instanceof DialResponse) {
            final DialResponse resp = (DialResponse)p;

            Runnable r = new Runnable() {
                @Override
                public void run() {
                    long time = System.currentTimeMillis();

                    while (System.currentTimeMillis() - time < 60000 && !opt.contains("COMM")) {
                        try {
                            binder.send(
                                    new EchoRequest().transactionId(transactionId).data(""),
                                    resp.getConfig().getAddress(),
                                    resp.getConfig().getPort());

                            Thread.sleep(500);
                        } catch (Throwable t) {
                            t.printStackTrace();
                        }
                    }

                    opt.add("END");
                }
            };

            new Thread(r).start();
        } else if (p instanceof EchoRequest) {
            opt.add("COMM");

            EchoRequest o = (EchoRequest)p;
            binder.send(
                    new EchoResponse().transactionId(o.getTransactionId()).data(o.getData()),
                    host,
                    port);
        }
    }
}
