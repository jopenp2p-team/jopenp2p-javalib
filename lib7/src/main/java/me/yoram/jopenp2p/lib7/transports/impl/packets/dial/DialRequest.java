/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.impl.packets.dial;

import me.yoram.jopenp2p.lib7.transports.api.AbstractServerRequest;
import me.yoram.jopenp2p.lib7.transports.api.PacketPrefix;
import me.yoram.jopenp2p.lib7.transports.api.EPacketType;
import me.yoram.jopenp2p.lib7.utils.ObjectUtils;
import me.yoram.jopenp2p.lib7.utils.ResizeableByteBuffer;

import java.io.IOException;
import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 17/07/18
 */
@PacketPrefix(EPacketType.DIAL_REQUEST)
public class DialRequest extends AbstractServerRequest {
    private String remoteUsername;

    public String getRemoteUsername() {
        return remoteUsername;
    }

    public DialRequest remoteUsername(final String remoteUsername) {
        this.remoteUsername = remoteUsername;

        return this;
    }

    @Override
    public DialRequest transactionId(final long transactionId) {
        return (DialRequest)super.transactionId(transactionId);
    }

    @Override
    protected short getLength() {
        return (short)(super.getLength() + this.remoteUsername.length() + 2);
    }

    @Override
    public DialRequest authid(final String authid) {
        return (DialRequest)super.authid(authid);
    }

    @Override
    public DialRequest appid(final String appid) {
        return (DialRequest)super.appid(appid);
    }

    @Override
    protected void load(final ResizeableByteBuffer buffer) throws IOException {
        super.load(buffer);
        remoteUsername = buffer.getString();
    }

    @Override
    protected void save(final ResizeableByteBuffer buffer) {
        super.save(buffer);
        buffer.putString(remoteUsername);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DialRequest) {
            final DialRequest o = (DialRequest)obj;

            return super.equals(o) && Objects.equals(this.remoteUsername, o.remoteUsername);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return ObjectUtils.hashCode(super.hashCode(), remoteUsername);
    }
}
