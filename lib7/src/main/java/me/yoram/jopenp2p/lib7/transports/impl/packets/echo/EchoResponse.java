/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.impl.packets.echo;

import me.yoram.jopenp2p.lib7.transports.api.EPacketType;
import me.yoram.jopenp2p.lib7.transports.api.PacketPrefix;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 18/07/18
 */
@PacketPrefix(EPacketType.ECHO_RESPONSE)
public class EchoResponse extends AbstractEchoPacket {
    @Override
    public EchoResponse transactionId(long transactionId) {
        return (EchoResponse)super.transactionId(transactionId);
    }

    @Override
    public EchoResponse data(String data) {
        return (EchoResponse)super.data(data);
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof EchoResponse && super.equals(obj));
    }
}
