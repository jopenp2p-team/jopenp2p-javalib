/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.impl.packets.echo;

import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.transports.api.EPacketType;
import me.yoram.jopenp2p.lib7.transports.api.PacketPrefix;
import me.yoram.jopenp2p.lib7.utils.ObjectUtils;
import me.yoram.jopenp2p.lib7.utils.ResizeableByteBuffer;

import java.io.IOException;
import java.util.Objects;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 18/07/18
 */
@PacketPrefix(EPacketType.ECHO_REQUEST)
public class AbstractEchoPacket extends AbstractPacket {
    private String data;

    public String getData() {
        return data;
    }

    public AbstractEchoPacket data(final String data) {
        this.data = data;

        return this;
    }

    @Override
    protected short getLength() {
        return (short)(this.data.length() + 2);
    }

    @Override
    public AbstractEchoPacket transactionId(long transactionId) {
        return (AbstractEchoPacket)super.transactionId(transactionId);
    }

    @Override
    protected void load(final ResizeableByteBuffer buffer) throws IOException {
        this.data = buffer.getString();
    }

    @Override
    public void save(ResizeableByteBuffer buffer) {
        buffer.putString(this.data);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AbstractEchoPacket) {
            final AbstractEchoPacket o = (AbstractEchoPacket)obj;

            return super.equals(obj) && Objects.equals(this.data, o.data);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return ObjectUtils.hashCode(super.hashCode(), data);
    }
}
