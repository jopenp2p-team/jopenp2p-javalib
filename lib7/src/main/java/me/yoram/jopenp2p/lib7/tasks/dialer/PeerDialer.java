/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.tasks.dialer;

import me.yoram.jopenp2p.lib7.api.PacketEvent;
import me.yoram.jopenp2p.lib7.api.RendezVousConfig;
import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.transports.impl.packets.dial.DialRequest;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 22/07/18
 */
public class PeerDialer implements Dialer {
    private final RendezVousConfig config;
    private final SocketBinder binder;

    public PeerDialer(final RendezVousConfig config, final SocketBinder binder) {
        super();

        this.config = config;
        this.binder = binder;
    }

    @Override
    public void dial(final String username, final DialerEvents events) throws IOException {
        final long transactionId = System.currentTimeMillis();
        final Set<String> opt = new ConcurrentSkipListSet<>();

        final PacketEvent event = new DialingEvents(opt, transactionId);

        AbstractPacket.registerEvent(transactionId, event);

        final DialRequest request = new DialRequest()
                .appid(config.getAppId())
                .authid(config.getAuthId())
                .remoteUsername(username)
                .transactionId(System.currentTimeMillis());

        config.getSocket().send(request, config.getHost(), config.getPort());

        while (!opt.contains("END")) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                // NO THING
            }
        }

        events.onCommunication(null);
    }

}
