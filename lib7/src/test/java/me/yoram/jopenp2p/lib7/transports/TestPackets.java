/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports;

import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.transports.api.EPacketType;
import me.yoram.jopenp2p.lib7.transports.api.PacketPrefix;
import me.yoram.jopenp2p.lib7.transports.impl.packets.ApplicationPacket;
import me.yoram.jopenp2p.lib7.transports.impl.packets.RegisterRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.dial.CallbackRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.dial.DialRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.dial.DialResponse;
import me.yoram.jopenp2p.lib7.transports.impl.packets.echo.EchoRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.echo.EchoResponse;
import me.yoram.jopenp2p.lib7.transports.impl.packets.list.ListRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.list.ListResponse;
import me.yoram.jopenp2p.lib7.utils.ResizeableByteBuffer;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.net.InetAddress;
import java.security.SecureRandom;
import java.util.UUID;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 18/07/18
 */
public class TestPackets {
    private static final SecureRandom RND = new SecureRandom();

    private static InetAddress randomIp() throws Exception {
        byte[] ip = new byte[4];

        RND.nextBytes(ip);

        return InetAddress.getByAddress(ip);
    }

    @DataProvider
    public Object[][] dpSaveAndLoad() throws Exception {
        return new Object[][] {
                {
                    new CallbackRequest()
                            .transactionId(RND.nextLong())
                            .remoteUsername(UUID.randomUUID().toString())
                            .config(new ResizeableByteBuffer.IPConfig().address(randomIp()).port(RND.nextInt())),
                        EPacketType.CALL_BACK_REQUEST
                },
                {
                        new DialRequest()
                                .transactionId(RND.nextLong())
                                .authid(UUID.randomUUID().toString())
                                .appid(UUID.randomUUID().toString())
                                .remoteUsername(UUID.randomUUID().toString()),
                        EPacketType.DIAL_REQUEST
                },
                {
                        new DialResponse()
                                .transactionId(RND.nextLong())
                                .remoteUsername(UUID.randomUUID().toString())
                                .config(new ResizeableByteBuffer.IPConfig().address(randomIp()).port(RND.nextInt())),
                        EPacketType.DIAL_RESPONSE
                },
                {
                        new EchoRequest()
                                .transactionId(RND.nextLong())
                                .data(UUID.randomUUID().toString()),
                        EPacketType.ECHO_REQUEST
                },
                {
                        new EchoResponse()
                                .transactionId(RND.nextLong())
                                .data(UUID.randomUUID().toString()),
                        EPacketType.ECHO_RESPONSE
                },
                {
                        new ListRequest()
                                .transactionId(RND.nextLong())
                                .appid(UUID.randomUUID().toString())
                                .authid(UUID.randomUUID().toString()),
                        EPacketType.LIST_REQUEST
                },
                {
                        new ListResponse()
                                .transactionId(RND.nextLong())
                                .remoteUsername(UUID.randomUUID().toString())
                                .config(new ResizeableByteBuffer.IPConfig().address(randomIp()).port(RND.nextInt())),
                        EPacketType.LIST_RESPONSE
                },
                {
                    new ApplicationPacket()
                            .transactionId(RND.nextLong())
                            .remoteUsername(UUID.randomUUID().toString())
                            .data(UUID.randomUUID().toString().getBytes()),
                        EPacketType.APPLICATION
                },
                {
                    new RegisterRequest()
                            .transactionId(RND.nextLong())
                            .appid(UUID.randomUUID().toString())
                            .authid(UUID.randomUUID().toString())
                            .username(UUID.randomUUID().toString()),
                        EPacketType.REGISTER_REQUEST
                },
        };
    }

    @Test(dataProvider = "dpSaveAndLoad")
    public void testPrefixes(AbstractPacket p, EPacketType expectedPacketType) throws Exception {
        PacketPrefix prefix = p.getClass().getAnnotation(PacketPrefix.class);

        assert prefix != null : String.format("%s should have an annotation for the prefix.", p.getClass().getName());
        assert prefix.value() == expectedPacketType :
                String.format(
                        "%s should have an annotation prefix of %s but had %s instead.",
                        p.getClass().getName(),
                        expectedPacketType.toString(),
                        prefix.value().toString());
    }

    @Test(dataProvider = "dpSaveAndLoad")
    public void testSaveAndLoad(AbstractPacket p, EPacketType expectedPacketType) throws Exception {
        byte[] data = p.save();
        assert data != null :
                String.format("%s returned null when saving data", p.getClass().getName());
        assert data.length > 0 :
                String.format("%s returned 0 length when saving data", p.getClass().getName());
        assert data[0] == expectedPacketType.getValue() :
                String.format(
                        "%s should return EPacketType.%s(%d) when saving data but returned byte %d",
                        p.getClass().getName(),
                        expectedPacketType.toString(),
                        expectedPacketType.getValue(),
                        data[0]);

        AbstractPacket loaded = p.getClass().newInstance();
        loaded.load(data, 0, data.length);

        assert p.equals(loaded) :
                String.format(
                        "%s is not equal when saved buffer is reloaded to new instance.\n%s\n%s",
                        p.getClass().getName(),
                        p.toString(),
                        loaded.toString());

    }
}
