/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.impl.events;

import me.yoram.jopenp2p.lib7.transports.impl.packets.echo.EchoRequest;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.util.Arrays;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 31/07/18
 */
public class TestSimpleDebugLoggerEvents {
    private static void checkContains(String s, String... data) {
        for (String o: data) {
            assert s.contains(o) : String.format("[%s] expected in %s", o, s);
        }
    }

    @Test
    public void testNoLogPacket() throws Exception {
        File logFile = File.createTempFile("TestSimpleDebugLoggerEvents", ".log");
        logFile.deleteOnExit();

        EchoRequest request = new EchoRequest().transactionId(123).data("456");

        try (
                OutputStream out = new FileOutputStream(logFile);
                PrintStream ps = new PrintStream(out)
        ) {
            SimpleDebugLoggerEvents events = new SimpleDebugLoggerEvents(ps);

            events.onPacket(request, InetAddress.getLocalHost(), 1234, null);
        }

        String res = FileUtils.readFileToString(logFile, "UTF-8");

        checkContains(
                res,
                EchoRequest.class.getSimpleName(),
                String.format("%s/1234", InetAddress.getLocalHost().getHostAddress()),
                request.toString());
    }

    @Test
    public void testLogPacket() throws Exception {
        File logFile = File.createTempFile("TestSimpleDebugLoggerEvents", ".log");
        logFile.deleteOnExit();

        File packetFolder = File.createTempFile("TestSimpleDebugLoggerEvents", ".packet");

        assert !packetFolder.exists() || packetFolder.delete() :
                "Could not erase the temp file, thus not create the packet folder.";

        assert packetFolder.mkdirs() : "Could not create the temp directory for packets.";

        EchoRequest request = new EchoRequest().transactionId(123).data("456");

        // Packet of type EchoRequest received from 127.0.1.1/1234. The packet has been stored in /tmp/TestSimpleDebugLoggerEvents4295214185587838358.packet/215c3a9e-c03a-4748-829a-602b056bd029.packet

        try (
                OutputStream out = new FileOutputStream(logFile);
                PrintStream ps = new PrintStream(out)
        ) {
            SimpleDebugLoggerEvents events = new SimpleDebugLoggerEvents(ps).logPacketFolder(packetFolder);

            events.onPacket(request, InetAddress.getLocalHost(), 1234, null);
        }

        File[] packets = packetFolder.listFiles();
        assert packets != null : "No packets written";
        try {
            assert packets.length == 1 : String.format("1 packet expected but %d written", packets.length);

            String res = FileUtils.readFileToString(logFile, "UTF-8");

            checkContains(
                    res,
                    EchoRequest.class.getSimpleName(),
                    String.format("%s/1234", InetAddress.getLocalHost().getHostAddress()),
                    packets[0].getAbsolutePath());

            byte[] expected = FileUtils.readFileToByteArray(packets[0]);
            assert Arrays.equals(expected, request.save()) :
                    String.format("Expected: %s\nResult: %s", new String(expected), new String(request.save()));
        } finally {
            for (File f: packets) {
                System.out.println(String.format("deleting %s", f.getAbsolutePath()));
                f.delete();
            }

            System.out.println(String.format("deleting %s", packetFolder.getAbsolutePath()));
            packetFolder.delete();
        }
    }
}
