/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.utils;

import org.testng.annotations.Test;

import java.net.InetAddress;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 16/07/18
 */
public class TestResizeableByteBuffer {
    @Test
    public void test() throws Exception {
        SecureRandom random = new SecureRandom();
        ResizeableByteBuffer buffer = new ResizeableByteBuffer(512);

        byte b = (byte)random.nextInt();
        buffer.put(b);

        byte[] buf = random.generateSeed(10);
        buffer.putByteArray(buf);

        short sh = (short)random.nextInt();
        buffer.putShort(sh);

        int i = random.nextInt();
        buffer.putInt(i);

        String s = UUID.randomUUID().toString();
        buffer.putString(s);

        ResizeableByteBuffer.IPConfig ip = new ResizeableByteBuffer.IPConfig()
                .address(InetAddress.getByAddress(new byte[] {1, 2, 3, 4}))
                .port(random.nextInt());
        buffer.putIpConfig(ip);

        buffer = new ResizeableByteBuffer(buffer.getData());

        assert b == buffer.get() : "byte not equals";

        byte[] buf2 = buffer.getByteArray();
        assert Arrays.equals(buf, buf2) : "byte[] not equals";

        assert sh == buffer.getShort() : "short not equals";
        assert i == buffer.getInt() : "int not equals";
        assert s.equals(buffer.getString()) : "String not equals";
        ResizeableByteBuffer.IPConfig ip2 = (ResizeableByteBuffer.IPConfig)buffer.getNext();
        assert ip.equals(ip2) : "ipconfig not equals";
        buffer.getNext();
    }
}
