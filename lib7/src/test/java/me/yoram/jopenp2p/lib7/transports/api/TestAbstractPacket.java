/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.api;

import me.yoram.jopenp2p.lib7.api.PacketEvent;
import me.yoram.jopenp2p.lib7.api.SocketBinder;
import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.util.List;
import java.util.Map;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 20/07/18
 */
public class TestAbstractPacket {
    @Test
    public void testRegister() {
        PacketEvent evt1 = new PacketEvent() {
            @Override
            public String toString() {
                return "EVT1";
            }

            @Override
            public void onPacket(AbstractPacket p, InetAddress host, int port, SocketBinder binder) throws IOException {

            }
        };
        AbstractPacket.registerEvent(EPacketType.values(), evt1);

        PacketEvent evt2 = new PacketEvent() {
            @Override
            public String toString() {
                return "EVT2";
            }

            @Override
            public void onPacket(AbstractPacket p, InetAddress host, int port, SocketBinder binder) throws IOException {

            }
        };
        AbstractPacket.registerEvent(EPacketType.values(), evt2);
        AbstractPacket.unregisterEvent(EPacketType.values(), evt1);

        Map<EPacketType, List<WeakReference<PacketEvent>>> map = AbstractPacket.getEVENTS();
        for (Map.Entry<EPacketType, List<WeakReference<PacketEvent>>> entry: map.entrySet()) {
            assert entry.getValue().size() == 1 :
                    String.format(
                            "List size should be one for packet of type %s: %s",
                            entry.getKey().toString(),
                            entry.getValue());


            PacketEvent event = entry.getValue().get(0).get();
            assert  event != null :
                    String.format(
                            "List size is one for packet of type %s but pointer is null", entry.getKey().toString());

            assert event.toString().equals("EVT2") :
                    String.format("Expected event EVT2 but %s returned instead.", event.toString());
        }
    }
}
