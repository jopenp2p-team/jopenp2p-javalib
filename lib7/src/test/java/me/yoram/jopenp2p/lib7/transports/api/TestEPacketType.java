/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.lib7.transports.api;

import org.testng.annotations.Test;

import java.util.Set;
import java.util.TreeSet;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 18/07/18
 */
public class TestEPacketType {
    @Test
    public void test() {
        Set<Byte> set = new TreeSet<>();

        for (EPacketType pt: EPacketType.values()) {
            assert !set.contains(pt.getValue()) :
                    String.format("Value %d exists twice in %s", pt.getValue(), pt.toString());

            set.add(pt.getValue());

            EPacketType pt2 = EPacketType.fromValue(pt.getValue());

            assert pt == pt2 : String.format(
                    "Value %s is numerical value %d but retrieving from that same numerical value gives %s.",
                    pt.toString(),
                    pt.getValue(),
                    pt2 == null ? "null" : pt2.toString());
        }
    }
}
