# jopenp2p-javalib #

Peer to peer communication is a subject that is often talked about but not very much understood. Most so called
peer to peer application relies on so called Relay Servers that act as a go between from one peer to the other which
requires heavy duty often expensive servers to handle the large amount of traffic. Real peer to peer communications
should send the packet directly to the recipient.
  
jopenp2p-javalib is an open source java library that allows peer to peer communication. It works with JDK 1.7 and 
Android. It provides an API that aim to handle and thus hide the intrinsic of peer ot peer communication from using 
application.

#### Hole Punching in a nutshell ####

> Networked devices with public or globally accessible IP addresses can create connections between one another easily. 
Clients with private addresses may also easily connect to public servers, as long as the client behind a router or 
firewall initiates the connection. However, hole punching (or some other form of NAT traversal) is required to 
establish a direct connection between two clients that both reside behind different firewalls or routers that use 
network address translation (NAT).
>
> Both clients initiate a connection to a RendezVous Server, which notes endpoint and session information including 
public IP and port along with private IP and port. The firewalls also note the endpoints in order to allow 
responses from the server to pass back through. The server then sends each client's endpoint and session 
information to the other client, or peer. Each client tries to connect to its peer through the specified IP address 
and port that the peer's firewall has open for the server. The new connection attempt punches a hole in the client's 
firewall as the endpoint now becomes open to receive a response from its peer. Depending on network conditions, one or 
both clients might receive a connection request. Successful exchange of an authentication nonce between both clients 
indicates the completion of a hole punching procedure. 

source: [wikipedia](https://en.wikipedia.org/wiki/Hole_punching_(networking))

### RendezVous Server ###

By connecting to a __RendezVous Server__, each peer will be able to send it's public ip address and port. Although there
are standard to do such thing such as Session Traversal Utilities for NAT (STUN) This library 
implements it own protocol to allow a richer API. A reference implementation of the RendezVous Server is also provided 
open source (see [jopenp2p-rendezvous](https://bitbucket.org/jopenp2p-team/jopenp2p-jrendezvous)). Alternatively you
can open a free account at [rendezvous.yoram.me](http://rendezvous.yoram.me) for development and move to subscription 
based as your application grows. It has security feature and scalable architecture to fit all of your needs. The 
jopenp2p-rendezvous will have more details on the difference between open source and commercial servers/  


### Dialing a Peer ###

Dialing a peer requires both peer to call each other on their UDP port, if one peer is under a NAT, the router has 80% 
changes to perform what is known as hole punching. For so called symmetric router, hole punching is impossible.

     +---------+     DialRequest     +-------------+             +-----------+
     |         +-------------------> |  RendezVous | Callback    |           |
     |         |                     |             +-----------> |           |
     |  Peer 1 |     DialResponse    |    Server   | Request     |   Peer 2  |
     |         | <-------------------+             |             |           |
     +---------+                     +-------------+             +-----------+

once both party has the public IP and port of the other they send EchoRequest to each other, when they get the 
EchoResponse it means they are connected.

                   
     +---------+    EchoRequest (from P1) +----------+
     |         +------------------------> |          |
     |  Peer 1 |                          | Peer 2   |
     |         |    EchoRequest (from P2) |          |
     |         | <------------------------+          |
     +---------+                          +----------+



### Special Thanks ###

 * http://asciiflow.com/ - for providing a platform for building diagrams in ASCII 