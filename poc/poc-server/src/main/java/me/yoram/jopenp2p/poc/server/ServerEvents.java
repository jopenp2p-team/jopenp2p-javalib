/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.server;

import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.api.PacketEvent;
import me.yoram.jopenp2p.lib7.transports.impl.packets.dial.CallbackRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.dial.DialRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.dial.DialResponse;
import me.yoram.jopenp2p.lib7.transports.impl.packets.list.ListRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.list.ListResponse;
import me.yoram.jopenp2p.lib7.transports.impl.packets.RegisterRequest;
import me.yoram.jopenp2p.lib7.utils.ResizeableByteBuffer;

import java.io.IOException;
import java.net.InetAddress;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 18/07/18
 */
public class ServerEvents implements PacketEvent {
    private void onPacket(RegisterRequest p, InetAddress host, int port) {
        SessionManager.addUser(host.getHostAddress(), port, p.getUsername());
    }

    private void onPacket(DialRequest p, InetAddress host, int port, SocketBinder binder) throws IOException {
        SessionManager.User u = SessionManager.getUser(p.getRemoteUsername());

        AbstractPacket p2 = new DialResponse()
                .remoteUsername(u.getUser())
                .config(new ResizeableByteBuffer
                        .IPConfig()
                        .address(InetAddress.getByName(u.getIp()))
                        .port(u.getPort()))
                .transactionId(p.getTransactionId());

        binder.send(p2, host, port);

        p2 = new CallbackRequest()
                .remoteUsername("unknown")
                .config(new ResizeableByteBuffer
                        .IPConfig()
                        .address(host)
                        .port(port))
                .transactionId(p.getTransactionId());

        binder.send(p2, InetAddress.getByName(u.getIp()), u.getPort());
    }

    private void onPacket(ListRequest p, InetAddress host, int port, SocketBinder binder) throws IOException {
        for (SessionManager.User u: SessionManager.getUsers()) {
            binder.send(
                    new ListResponse()
                            .transactionId(p.getTransactionId())
                            .remoteUsername(u.getUser())
                            .config(new ResizeableByteBuffer.IPConfig().address(InetAddress.getByName(u.getIp())).port(u.getPort())),
                    host,
                    port);
        }
    }

    @Override
    public void onPacket(AbstractPacket p, InetAddress host, int port, SocketBinder binder) throws IOException {
        if (p instanceof RegisterRequest) {
            onPacket((RegisterRequest)p, host, port);
        } else if (p instanceof DialRequest) {
            onPacket((DialRequest)p, host, port, binder);
        } else if (p instanceof ListRequest) {
            onPacket((ListRequest)p, host, port, binder);
        }
    }
}
