/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.server;

import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.transports.api.EPacketType;
import me.yoram.jopenp2p.lib7.tasks.listeners.udp.UdpListener;
import me.yoram.jopenp2p.lib7.transports.impl.events.SimpleDebugLoggerEvents;
import me.yoram.jopenp2p.lib7.transports.impl.events.SimpleEchoRequestEvent;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

public class Server {
    private static void printHelp() {
        System.out.println("Usage: java -jar server.jar [port=5666]");
        System.out.println();
        System.exit(10);
    }

    private static Method findMethod(Class<?> clazz, String methodName, Class<?>... params) {
        try {
            return clazz.getMethod(methodName, params);
        } catch (Throwable t) {
            return null;
        }
    }

    private static Method findMethod(Class<?> clazz, String methodName) {
        Method res = findMethod(clazz, methodName, String.class);

        if (res == null) {
            res = findMethod(clazz, methodName, boolean.class);
        }

        if (res == null) {
            res = findMethod(clazz, methodName, int.class);
        }

        return res;
    }

    private static void processCommandLine(Object o, String[] args) throws Exception {
        if (args != null) {
            for (String s: args) {
                String[] parts = s.split("=");

                if (parts.length > 2) {
                    throw new Exception(String.format("%s is not a valid argument", s));
                }

                Method m;

                if (parts.length == 2) {
                    m = findMethod(o.getClass(), parts[0]);
                } else {
                    m = findMethod(o.getClass(), parts[0], boolean.class);
                }

                if (m == null) {
                    throw new Exception(String.format("%s is not a valid argument", s));
                }

                if (parts.length == 2) {
                    if (m.getParameterTypes()[0].equals(String.class)) {
                        m.invoke(o, parts[1]);
                    } else if (m.getParameterTypes()[0].equals(boolean.class)) {
                        m.invoke(o, parts[1].equals("true"));
                    } else if (m.getParameterTypes()[0].equals(int.class)) {
                        m.invoke(o, Integer.parseInt(parts[1]));
                    }
                } else {
                    m.invoke(o, true);
                }
            }
        }
    }

    public static void main(String args[]) throws Exception {
        try {
            SessionManager.getUsers();

            Server server = new Server();
            processCommandLine(server, args);

            server.run();
        } catch (Throwable t) {
            t.printStackTrace();
            System.out.println();
            printHelp();
        }

    }

	private int port = 5666;

    public Server() {
        super();

        AbstractPacket.registerEvent(EPacketType.values(), new ServerEvents());

        File packetFolder = new File(new File(new File(new File(System.getProperty("user.home")), ".jopenp2p"), ".poc-server"), ".packet");
        packetFolder.mkdirs();
        System.out.println(String.format("Packet folder in %s", packetFolder.getAbsolutePath()));

        AbstractPacket.registerEvent(
                EPacketType.values(),
                new SimpleDebugLoggerEvents(System.out).logPacketFolder(packetFolder));
        AbstractPacket.registerEvent(EPacketType.ECHO_REQUEST, new SimpleEchoRequestEvent());
    }


	public Server port(int port) {
		this.port = port;

		return this;
	}

	public void run() throws IOException {
	    Thread t = new Thread(new UdpListener(this.port));
	    t.setDaemon(false);
	    t.start();
	}
}

