/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.server;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 09/07/18
 */
public class SessionManager {
    private static final Map<String, User> USERS = new ConcurrentHashMap<>();

    static class User {
        private long lastchecked;
        private String ip;
        private int port;
        private String user;

        String getIp() {
            return ip;
        }

        int getPort() {
            return port;
        }

        String getUser() {
            return user;
        }
    }

    private static class ClearSessionRunnable implements Runnable {
        private boolean finished = false;
        @Override
        public void run() {
            List<String> list = new ArrayList<>();

            while (true) {
                try {
                    for (final Map.Entry<String, User> user: USERS.entrySet()) {
                        if (user.getValue().lastchecked < System.currentTimeMillis() - 60000) {
                            list.add(user.getKey());
                        }
                    }

                    for (String key: list) {
                        USERS.remove(key);
                    }

                    list.clear();

                    Thread.sleep(60000);

                    if (finished) {
                        break;
                    }
                } catch (Throwable t) {
                    // DO NOTHING
                }
            }
        }
    }

    static {
        new Thread(new ClearSessionRunnable()).start();
    }

    static User addUser(final String ip, final int port, final String user) {
        final String key = ip + "/" + port;

        final User res;

        if (USERS.containsKey(key)) {
            res = USERS.get(key);
            res.lastchecked = System.currentTimeMillis();
        } else {
            res = new User();
            res.ip = ip;
            res.port = port;
            res.user = user;
            res.lastchecked = System.currentTimeMillis();

            USERS.put(key, res);
        }

        return res;
    }

    static List<User> getUsers() {
        return Arrays.asList(USERS.values().toArray(new User[0]));
    }

    static User getUser(final String username) {
        for (final User u: getUsers()) {
            if (username.equals(u.getUser())) {
                return u;
            }
        }

        return null;
    }
}
