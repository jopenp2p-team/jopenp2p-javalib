/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.client;

import me.yoram.jopenp2p.lib7.api.RendezVousConfig;
import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.transports.api.EPacketType;
import me.yoram.jopenp2p.lib7.tasks.listeners.udp.UdpListener;
import me.yoram.jopenp2p.lib7.tasks.register.RegisterClientTask;
import me.yoram.jopenp2p.poc.client.commands.*;
import me.yoram.jopenp2p.poc.commons.cmd.api.CommandLineApplication;
import me.yoram.jopenp2p.poc.commons.cmd.api.ICommand;
import me.yoram.jopenp2p.poc.commons.cmd.impl.QuitCommand;
import me.yoram.jopenp2p.poc.commons.cmd.impl.UnsupportedCommand;
import me.yoram.jopenp2p.poc.commons.exceptions.BusinessError;
import me.yoram.jopenp2p.poc.commons.utils.ClassUtils;
import me.yoram.jopenp2p.poc.commons.utils.LoggerUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.*;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 06/07/18
 */
public class Client extends CommandLineApplication<Config> {
    private static final ICommand[] COMMANDS = new ICommand[] {
            new SendCommand(), new QuitCommand(), new EchoCommand(), new SetCommand(), new DialCommand(),
            new UnsupportedCommand()};


    private static String[] readLine(BufferedReader reader) throws IOException {
        System.out.print("> ");

        String s = reader.readLine();
        List<String> res = new ArrayList<>();

        for (String s2: s.split(" ")) {
            s2 = s2.trim();

            if (!s.isEmpty()) {
                res.add(s2);
            }
        }

        return res.toArray(new String[0]);
    }


    public static void main(String args[]) throws Exception {
        Client client = new Client();
        ClassUtils.processCommandLine(Config.class, null, args);
        client.run(args);
    }

    @Override
    protected Config getConfig() {
        return new Config();
    }

    @Override
    public void doRun() throws Exception {
        if (!Config.getHomeFolder().exists() && !Config.getHomeFolder().mkdirs()) {
            throw new Exception(String.format(
                    "Couldn't create home folder at %s", Config.getHomeFolder().getAbsolutePath()));
        }
        LoggerUtils.info("User Home: " + Config.getHomeFolder().getAbsolutePath());

        File packetFolder = new File(Config.getHomeFolder(), ".packets");
        if (!packetFolder.exists() && !packetFolder.mkdirs()) {
            throw new Exception(String.format(
                    "Couldn't create packets folder at %s", packetFolder.getAbsolutePath()));
        }
        LoggerUtils.info("Packets folder: " + Config.getHomeFolder().getAbsolutePath());

        //AbstractPacket.registerEvent(EPacketType.values(), new SimpleDebugLoggerEvents(packetFolder));
        AbstractPacket.registerEvent(EPacketType.values(), new ClientEvents());
        //AbstractPacket.registerEvent(EPacketType.ECHO_REQUEST, new SimpleEchoRequestEvent());

        final UdpListener listener = new UdpListener(Config.getPort());
        new Thread(listener).start();

        new Timer().schedule(new RegisterClientTask(
                new RendezVousConfig() {
                    @Override
                    public SocketBinder getSocket() {
                        return listener;
                    }

                    @Override
                    public String getUsername() {
                        return Config.getUsername();
                    }

                    @Override
                    public InetAddress getHost() {
                        return Config.getcServerIp();
                    }

                    @Override
                    public int getPort() {
                        return Config.getCport();
                    }

                    @Override
                    public String getAuthId() {
                        return "ABCD";
                    }

                    @Override
                    public String getAppId() {
                        return "1234";
                    }
                }),
                0,
                5000);

        System.out.println(
                "Client is started and listening! " +
                        listener.getHostAddress() + "/" +
                        listener.getPort());
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] command;

        while (true) {
            command = readLine(reader);

            if (command.length == 0) {
                continue;
            }

            for (ICommand c: COMMANDS) {
                if (c.handles(command)) {
                    try {
                        c.run(command, listener);
                    } catch (BusinessError error) {
                        System.err.println(String.format("Error: %s", error.getMessage()));
                        c.help();
                        System.out.println();
                    } catch (Throwable t) {
                        System.err.println(String.format("Error: %s", t.getMessage()));
                        t.printStackTrace();
                        System.err.println();
                        c.help();
                        System.out.println();
                    }

                    break;
                }
            }
        }
    }

    @Override
    protected void printHelp() {
        System.out.println("Usage: java -jar client.jar [port=19876] [socketreuse=false]");
        System.out.println();
    }

}
