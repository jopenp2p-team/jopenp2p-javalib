/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.client.commands;

import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.lib7.transports.impl.packets.echo.EchoRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.echo.EchoResponse;
import me.yoram.jopenp2p.poc.commons.cmd.api.ICommand;

import java.net.InetAddress;

/**
 * comm rdvserver rdvport peerserver peerport count sleepms message
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 06/07/18
 */
public class EchoCommand implements ICommand {
    @Override
    public boolean handles(String[] command) {
        return command != null && command.length > 0 && command[0].equals("echo");
    }

    @Override
    public void run(String[] command, final SocketBinder socket) throws Exception {
        if (command.length != 4) {
            throw new Exception("Failed!! ex: echo server port");
        }

        final InetAddress ip;

        try {
            ip = InetAddress.getByName(command[1]);
        } catch (Throwable t) {
            throw new Exception(String.format("Searching for ip [%s] throws error %s", command[1], t.getMessage()));
        }


        final int port;

        try {
            port = Integer.parseInt(command[2]);
        } catch (Throwable t) {
            throw new Exception(String.format("Convert port [%s] to int throws error %s", command[2], t.getMessage()));
        }

        socket.send(new EchoRequest().transactionId(System.currentTimeMillis()).data(command[3]), ip, port);
    }

    @Override
    public void help() {
        System.out.println("Example: echo server port");
    }
}
