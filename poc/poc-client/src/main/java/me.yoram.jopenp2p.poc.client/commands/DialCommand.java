/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.client.commands;

import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.transports.impl.packets.dial.DialRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.list.ListRequest;
import me.yoram.jopenp2p.poc.client.Config;
import me.yoram.jopenp2p.poc.commons.cmd.api.ICommand;

import java.net.InetAddress;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 06/07/18
 */
public class DialCommand implements ICommand {
    @Override
    public boolean handles(String[] command) {
        return command != null && command.length > 0 && command[0].equals("dial");
    }

    @Override
    public void run(String[] command, SocketBinder socket) throws Exception {
        if (command.length < 2) {
            throw new Exception("Failed!! ex: send ip port message");
        }

        StringBuilder msg = new StringBuilder();
        for (int i = 1; i < command.length; i++) {
            msg.append(" ");
            msg.append(command[i]);
        }


        DialRequest request = new DialRequest()
                .appid("123")
                .authid("123")
                .remoteUsername(msg.toString().trim())
                .transactionId(System.currentTimeMillis());

        socket.send(request, Config.getcServerIp(), Config.getCport());
    }

    @Override
    public void help() {
        System.out.println("Example: dial USER");
    }
}
