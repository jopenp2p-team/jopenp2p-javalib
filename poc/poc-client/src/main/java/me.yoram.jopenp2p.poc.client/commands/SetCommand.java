/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.client.commands;

import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.poc.client.Config;
import me.yoram.jopenp2p.poc.commons.cmd.api.ICommand;
import me.yoram.jopenp2p.poc.commons.exceptions.BusinessError;
import me.yoram.jopenp2p.poc.commons.utils.ClassUtils;
import me.yoram.jopenp2p.poc.commons.utils.ObjectUtils;

import java.net.DatagramSocket;
import java.util.Map;
import java.util.Properties;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 10/07/18
 */
public class SetCommand implements ICommand {
    @Override
    public boolean handles(final String[] command) {
        return command != null && command.length > 0 && command[0].equals("set");
    }

    @Override
    public void run(final String[] command, final SocketBinder socket) throws Exception {
        if (command.length == 1) {
            final Properties props = new Properties();
            Config.save(props);

            for (final Map.Entry<Object, Object> entry: props.entrySet()) {
                System.out.println(entry.getKey() + "=" + entry.getValue());
            }

            return;
        }

        if (command.length == 2) {
            if (command[1].equals("save")) {
                Config.save();

                return;
            } else if (command[1].equals("load")) {
                Config.load(false);

                return;
            }
        }

        if (command.length < 2) {
            throw new BusinessError(
                    String.format(
                            "Couldn't process %s, it must be in format set key=value",
                            ObjectUtils.flatten(command, " ")));
        }

        final String cmd = ObjectUtils.flatten(command, 1, " ");
        final int pos = cmd.indexOf("=");

        if (pos == -1) {
            throw new BusinessError(
                    String.format(
                            "Couldn't process %s, it must be in format set key=value",
                            ObjectUtils.flatten(command, " ")));
        }

        ClassUtils.processCommandLine(Config.class, null, new String[] {cmd});
    }

    @Override
    public void help() {
        System.err.println("example set key=value");
    }
}
