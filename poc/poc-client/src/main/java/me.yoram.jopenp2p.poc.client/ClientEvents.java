/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.client;

import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.lib7.transports.api.AbstractPacket;
import me.yoram.jopenp2p.lib7.api.PacketEvent;
import me.yoram.jopenp2p.lib7.transports.impl.packets.dial.DialResponse;
import me.yoram.jopenp2p.lib7.transports.impl.packets.echo.EchoRequest;
import me.yoram.jopenp2p.lib7.transports.impl.packets.echo.EchoResponse;
import me.yoram.jopenp2p.lib7.transports.impl.packets.list.ListResponse;

import java.io.IOException;
import java.net.InetAddress;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 19/07/18
 */
public class ClientEvents implements PacketEvent {
    public void onPacket(ListResponse p) {
        System.out.println(
                p.getRemoteUsername() +
                        " (" + p.getConfig().getAddress().getHostAddress() + "/" + p.getConfig().getPort() + ")");
    }

    public void onPacket(final DialResponse p, final SocketBinder binder) {
        final PacketEvent event = new PacketEvent() {
            @Override
            public void onPacket(AbstractPacket p, InetAddress host, int port, SocketBinder binder) throws IOException {
                System.out.println("COMM");
                if (p instanceof EchoRequest) {
                    EchoRequest o = (EchoRequest)p;
                    binder.send(
                            new EchoResponse().transactionId(o.getTransactionId()).data(o.getData()),
                            host,
                            port);
                }
            }
        };
        AbstractPacket.registerEvent(p.getTransactionId(), event);

        Runnable r = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    try {
                        binder.send(
                                new EchoRequest().transactionId(p.getTransactionId()).data(""),
                                p.getConfig().getAddress(),
                                p.getConfig().getPort());

                        Thread.sleep(500);
                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
            }
        };

        new Thread(r).start();
    }

    @Override
    public void onPacket(AbstractPacket p, InetAddress host, int port, SocketBinder binder) {
        if (p instanceof ListResponse) {
            onPacket((ListResponse)p);
        } else if (p instanceof DialResponse) {
            onPacket((DialResponse)p, binder);
        }
    }
}
