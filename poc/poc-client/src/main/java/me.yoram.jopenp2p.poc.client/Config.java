/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.client;

import me.yoram.jopenp2p.poc.commons.exceptions.BusinessError;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Properties;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 10/07/18
 */
public class Config {
    private static File homeFolder = new File(new File(System.getProperty("user.home")), ".jopenp2p");
    private static String cserver;
    private static InetAddress cServerIp;
    private static int cport = -1;
    private static String username;

    private static int port = 19876;
    private static boolean socketreuse = false;

    static {
        try {
            load(true);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    public static void save(final Properties props) {
        props.setProperty("cserver", cserver == null ? "" : cserver);
        props.setProperty("cport", "" + cport);
        props.setProperty("username", username == null ? "" : username);
        props.setProperty("port", "" + port);
        props.setProperty("socketreuse", socketreuse ? "true" : "false");
    }

    public static void save() throws IOException {
        final Properties props = new Properties();
        save(props);

        try (OutputStream out = new FileOutputStream(new File(Config.getHomeFolder(), "set.properties"))) {
            props.store(out, new Date().toString());
        }
    }

    public static void load(boolean init) throws BusinessError, IOException {
        final File f = new File(Config.getHomeFolder(), "set.properties");

        if (!f.exists()) {
            if (!init) {
                throw new BusinessError("No properties were saved!");
            } else {
                return;
            }
        }

        final Properties props = new Properties();

        try (final InputStream in = new FileInputStream(f)) {
            props.load(in);
        }

        if (props.containsKey("cserver")) {
            try {
                cserver(props.getProperty("cserver"));
            } catch (Throwable t) {
                System.err.println(
                        String.format(
                                "cserver property is [%s] but is invalid, resetting it to null",
                                props.getProperty("cserver")));
                cserver = null;
                cServerIp = null;
            }
        }

        if (props.containsKey("cport")) {
            try {
                cport(Integer.parseInt(props.getProperty("cport")));
            } catch (Throwable t) {
                System.err.println(
                        String.format(
                                "cport property is [%s] but is invalid, resetting it to -1",
                                props.getProperty("cport")));
                cport = -1;
            }
        }

        if (props.containsKey("username")) {
            username(props.getProperty("username"));
        }

        if (props.containsKey("port")) {
            try {
                port = Integer.parseInt(props.getProperty("port"));
            } catch (Throwable t) {
                System.err.println(
                        String.format(
                                "port property is [%s] but is invalid, resetting it to 19876",
                                props.getProperty("port")));
                port = 19876;
            }
        }

        if (props.containsKey("socketreuse")) {
            socketreuse = props.getProperty("socketreuse").equalsIgnoreCase("true");
        }
    }

    public static String getUsername() {
        return username;
    }

    public static void username(final String username) {
        Config.username = username;
    }

    public static String getCserver() {
        return cserver;
    }

    public static void cserver(String cserver) throws UnknownHostException {
        cServerIp = InetAddress.getByName(cserver);
        Config.cserver = cserver;
    }

    public static int getCport() {
        return cport;
    }

    public static void cport(int cport) {
        Config.cport = cport;
    }

    public static File getHomeFolder() {
        return homeFolder;
    }

    public static void homeFolder(File homeFolder) {
        Config.homeFolder = homeFolder;
    }

    public static int getPort() {
        return port;
    }

    public static void port(int port) {
        Config.port = port;
        System.out.println("You need to save. Changing port requires a restart");
    }

    public static boolean isSocketreuse() {
        return socketreuse;
    }

    public static void socketreuse(boolean socketreuse) {
        Config.socketreuse = socketreuse;
        System.out.println("You need to save. Changing socketreuse requires a restart");
    }

    public static InetAddress getcServerIp() {
        return cServerIp;
    }
}
