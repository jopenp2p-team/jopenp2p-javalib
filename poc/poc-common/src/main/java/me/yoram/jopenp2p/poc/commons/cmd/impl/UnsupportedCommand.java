/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.commons.cmd.impl;

import me.yoram.jopenp2p.lib7.api.SocketBinder;
import me.yoram.jopenp2p.poc.commons.cmd.api.ICommand;
import me.yoram.jopenp2p.poc.commons.exceptions.BusinessError;
import me.yoram.jopenp2p.poc.commons.utils.ObjectUtils;

import java.net.DatagramSocket;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 11/07/18
 */
public class UnsupportedCommand implements ICommand {
    @Override
    public boolean handles(String[] command) {
        return true;
    }

    @Override
    public void run(String[] command, SocketBinder socket) throws BusinessError {
        throw new BusinessError("Unsupported " + ObjectUtils.flatten(command, " "));
    }

    @Override
    public void help() {
        // DO NOTHING
    }
}
