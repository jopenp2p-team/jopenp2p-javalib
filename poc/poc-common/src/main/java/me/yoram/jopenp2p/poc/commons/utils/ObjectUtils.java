/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.commons.utils;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 11/07/18
 */
public class ObjectUtils {
    public static String flatten(String[] strs, int from, int to, String separator) {
        final StringBuilder sb = new StringBuilder();

        for (int i = from; i < to; i++) {
            final String s = strs[i] == null ? "" : strs[i].trim();

            if (!s.isEmpty()) {
                if (sb.length() > 0) {
                    sb.append(separator);
                }

                sb.append(s);
            }
        }

        return sb.toString();
    }

    public static String flatten(String[] strs, int from, String separator) {
        return flatten(strs, from, strs.length, separator);
    }

    public static String flatten(String[] strs, String separator) {
        return flatten(strs, 0, strs.length, separator);
    }
}
