/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.commons.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 10/07/18
 */
public class ClassUtils {
    private static final Class<?>[] DEFAULT_ALLOWED_FIELD_TYPE = {boolean.class, int.class, String.class};

    public static Set<String> getProperties(final Class<?> clazz, Object o) {
        final Set<String> res = new HashSet<>();

        for (final Field f: clazz.getDeclaredFields()) {
            for (Class<?> type: DEFAULT_ALLOWED_FIELD_TYPE) {
                if (type.equals(f.getType()) && (Modifier.isStatic(f.getModifiers()) || o != null)) {
                    res.add(f.getName());
                }
            }
        }

        return res;
    }

    public static Set<String> getProperties(final Class<?> clazz) {
        return getProperties(clazz, null);
    }

    public static Method findMethod(Class<?> clazz, String methodName, Class<?>... params) {
        try {
            return clazz.getMethod(methodName, params);
        } catch (Throwable t) {
            return null;
        }
    }

    public static Method findMethod(Class<?> clazz, String methodName) {
        Method res = findMethod(clazz, methodName, String.class);

        if (res == null) {
            res = findMethod(clazz, methodName, boolean.class);
        }

        if (res == null) {
            res = findMethod(clazz, methodName, int.class);
        }

        return res;
    }

    public static void processCommandLine(Class<?> clazz, final Object o, final String[] args) throws Exception {
        if (args == null || args.length == 0) {
            return;
        }

        if (clazz == null && o == null) {
            throw new IllegalArgumentException("clazz parameter should not be null or o parameter should be set.");
        } else if (clazz == null) {
            clazz = o.getClass();
        }

        final Properties props = o instanceof Properties ? (Properties)o : null;
        final Set<String> propNames = props == null ? getProperties(clazz, o) : null;

        if (props == null && propNames == null) {
            throw new IllegalArgumentException(
                    "The only configuration handles is java.util.Properties or a proper object.");
        }

        for (final String arg: args) {
            final int pos = arg.indexOf("=");

            if (pos == -1) {
                if (props != null) {
                    props.setProperty(arg, "true");
                } else {
                    final Method m = findMethod(clazz, arg, boolean.class);

                    if (m == null) {
                        throw new Exception(String.format("property [%s] does not exists.", arg));
                    }

                    m.invoke(o, true);
                }
            } else {
                String name = arg.substring(0, pos).trim();

                Method m = findMethod(clazz, name);

                if (m == null) {
                    throw new Exception(String.format("property [%s] does not exists.", name));
                }

                String value = arg.substring(pos + 1).trim();

                if (m.getParameterTypes()[0].equals(String.class)) {
                    m.invoke(o, value);
                } else if (m.getParameterTypes()[0].equals(boolean.class)) {
                    m.invoke(o, value.equalsIgnoreCase("true"));
                } else if (m.getParameterTypes()[0].equals(int.class)) {
                    m.invoke(o, Integer.parseInt(value));
                }
            }
        }
    }

}
