/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jopenp2p.poc.commons;

import me.yoram.jopenp2p.poc.commons.utils.ClassUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 10/07/18
 */
public class TestClassUtils {
    public static class TestClassO1 {
        private static boolean b1;
        private static Boolean b2;
        private boolean b3;
        private Boolean b4;
        private static int i1;
        private static Integer i2;
        private int i3;
        private Integer i4;
        private static String s1;
        private String s2;
        private static Object o1;
        private Object o2;

        public static void b1(boolean b1) {
            TestClassO1.b1 = b1;
        }

        public static void b2(Boolean b2) {
            TestClassO1.b2 = b2;
        }

        public void b3(boolean b3) {
            this.b3 = b3;
        }

        public void b4(Boolean b4) {
            this.b4 = b4;
        }

        public static void i1(int i1) {
            TestClassO1.i1 = i1;
        }

        public static void i2(Integer i2) {
            TestClassO1.i2 = i2;
        }

        public void i3(int i3) {
            this.i3 = i3;
        }

        public void i4(Integer i4) {
            this.i4 = i4;
        }

        public static void s1(String s1) {
            TestClassO1.s1 = s1;
        }

        public void s2(String s2) {
            this.s2 = s2;
        }

        public static void o1(Object o1) {
            TestClassO1.o1 = o1;
        }

        public void o2(Object o2) {
            this.o2 = o2;
        }
    }

    @DataProvider
    public Object[][] dpGetProperties() {
        return new Object[][] {
                {
                    TestClassO1.class,
                        null,
                        new HashSet<>(Arrays.asList("b1", "i1", "s1"))
                },
                {
                    TestClassO1.class,
                        new TestClassO1(),
                        new HashSet<>(Arrays.asList("b1", "b3", "i1", "i3", "s1", "s2"))
                }
        };
    }

    @Test(dataProvider = "dpGetProperties")
    public void testGetProperties(Class<?> clazz, Object instance, Set<String> controlSet) {
        Set<String> properties = ClassUtils.getProperties(TestClassO1.class, instance);
        Set<String> difference = new HashSet<>(properties);
        difference.removeAll(controlSet);

        assert difference.size() == 0 : String.format("%s properties should not have been returned.", difference);
    }

    @DataProvider
    public Object[][] dpFindMethod() {
        return new Object[][] {
                {TestClassO1.class, "b1", boolean.class, true},
                {TestClassO1.class, "b2", null, true},
                {TestClassO1.class, "b3", boolean.class, false},
                {TestClassO1.class, "b4", null, false},
                {TestClassO1.class, "i1", int.class, true},
                {TestClassO1.class, "i2", null, true},
                {TestClassO1.class, "i3", int.class, false},
                {TestClassO1.class, "i4", null, false},
                {TestClassO1.class, "s1", String.class, true},
                {TestClassO1.class, "s2", String.class, false},
                {TestClassO1.class, "o1", null, false},
                {TestClassO1.class, "o2", null, false},
                {TestClassO1.class, "notexists", null, false}
        };
    }

    @Test(dataProvider = "dpFindMethod")
    public void testFindMethod(Class<?> clazz, String methodName, Class<?> paramType, boolean isStatic) {
        Method m = ClassUtils.findMethod(clazz, methodName);

        if (paramType == null) {
            assert m == null : String.format("It should not have found a method but found %s", m);
        } else {
            assert m != null : String.format("No method found for [%s]", methodName);

            assert m.getParameterTypes().length == 1 :
                    String.format(
                            "Expected one parameter but %d returned for method %s", m.getParameterTypes().length, m);

            assert paramType.equals(m.getParameterTypes()[0]) :
                    String.format("Parameter type expected %s but %s returned", paramType, m.getParameterTypes()[0]);

            if (isStatic) {
                assert Modifier.isStatic(m.getModifiers()) : "Static method expected";
            } else {
                assert !Modifier.isStatic(m.getModifiers()) : "NON Static method expected";
            }
        }
    }

    @Test
     public void testProcessCommandLine() throws Exception {
        TestClassO1.b1 = false;

        ClassUtils.processCommandLine(TestClassO1.class, null, new String[] {"b1", "i1=10", "s1=ezr re  rez"});
        assert TestClassO1.b1 : "b1 should be true";
        assert TestClassO1.i1  == 10 : "i1 should be 10";
        assert TestClassO1.s1.equals("ezr re  rez") : "i1 should be [ezr re  rez]";
     }

    @Test
     public void testProcessCommandLineWithInstance() throws Exception {
        TestClassO1.b1 = false;
        TestClassO1 o = new TestClassO1();

        ClassUtils.processCommandLine(
                TestClassO1.class, o, new String[] {"b1", "b3", "i1=10", "i3=10", "s1=ezr re  rez", "s2=ezr re  rez"});
        assert TestClassO1.b1 : "b1 should be true";
        assert o.b3 : "b3 should be true";
        assert TestClassO1.i1  == 10 : "i1 should be 10";
        assert o.i3 == 10 : "i1 should be 10";
        assert TestClassO1.s1.equals("ezr re  rez") : "i1 should be [ezr re  rez]";
        assert o.s2.equals("ezr re  rez") : "i1 should be [ezr re  rez]";
     }
}
